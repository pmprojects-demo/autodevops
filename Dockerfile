FROM amazoncorretto:11

WORKDIR /usr/src/app

COPY . /usr/src/app
RUN chmod +x mvnw
RUN ./mvnw package

ENV PORT 5000
EXPOSE $PORT
CMD [ "sh", "-c", "./mvnw -Dserver.port=${PORT} spring-boot:run" ]
